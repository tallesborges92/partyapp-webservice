# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150505005132) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "agendas", force: :cascade do |t|
    t.integer  "enterprise_id"
    t.datetime "start_date"
    t.string   "description"
    t.string   "imageUrl"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "event_description"
    t.datetime "end_date"
    t.string   "event_name"
  end

  add_index "agendas", ["enterprise_id"], name: "index_agendas_on_enterprise_id"

  create_table "devices", force: :cascade do |t|
    t.integer  "enterpise_id"
    t.string   "token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "device_os"
  end

  add_index "devices", ["enterpise_id"], name: "index_devices_on_enterpise_id"

  create_table "enterprises", force: :cascade do |t|
    t.string   "name"
    t.string   "addres"
    t.string   "logoUrl"
    t.string   "telephone"
    t.string   "description"
    t.string   "hashtag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "lat"
    t.float    "lng"
    t.string   "website"
    t.string   "club_image"
  end

  create_table "feeds", force: :cascade do |t|
    t.integer  "enterprise_id"
    t.date     "day"
    t.string   "image_url"
    t.string   "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "feeds", ["enterprise_id"], name: "index_feeds_on_enterprise_id"

  create_table "masks", force: :cascade do |t|
    t.integer  "enterprise_id"
    t.string   "imageUrl"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "thumbUrl"
  end

  add_index "masks", ["enterprise_id"], name: "index_masks_on_enterprise_id"

  create_table "news", force: :cascade do |t|
    t.integer  "enterprise_id"
    t.string   "imageUrl"
    t.string   "description"
    t.date     "day"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "news", ["enterprise_id"], name: "index_news_on_enterprise_id"

  create_table "notifications", force: :cascade do |t|
    t.integer  "enterprise_id"
    t.string   "text"
    t.datetime "date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "notifications", ["enterprise_id"], name: "index_notifications_on_enterprise_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "enterprise_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["enterprise_id"], name: "index_users_on_enterprise_id"
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
