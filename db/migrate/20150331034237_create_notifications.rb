class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :enterprise, index: true
      t.string :text
      t.datetime :date

      t.timestamps null: false
    end
    add_foreign_key :notifications, :enterprises
  end
end
