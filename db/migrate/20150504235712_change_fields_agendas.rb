class ChangeFieldsAgendas < ActiveRecord::Migration
  def change
    rename_column :agendas, :day, :start_date
    change_column :agendas, :start_date, :datetime
    add_column :agendas, :end_date, :datetime
    add_column :agendas, :agenda_name, :string
  end
end
