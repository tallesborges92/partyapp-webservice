class AddClubImageToEnterprises < ActiveRecord::Migration
  def change
    add_column :enterprises, :club_image, :string
  end
end
