class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.references :enterpise, index: true
      t.string :token

      t.timestamps null: false
    end
    add_foreign_key :devices, :enterpises
  end
end
