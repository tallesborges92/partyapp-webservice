class AddThumbUrlToMask < ActiveRecord::Migration
  def change
    add_column :masks, :thumbUrl, :string
  end
end
