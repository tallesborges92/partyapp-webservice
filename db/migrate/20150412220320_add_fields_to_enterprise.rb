class AddFieldsToEnterprise < ActiveRecord::Migration
  def change
    add_column :enterprises, :lat, :float
    add_column :enterprises, :lng, :float
    add_column :enterprises, :website, :string
  end
end
