class CreateEnterprises < ActiveRecord::Migration
  def change
    create_table :enterprises do |t|
      t.string :name
      t.string :addres
      t.string :logoUrl
      t.string :telephone
      t.string :description
      t.string :hashtag

      t.timestamps null: false
    end
  end
end
