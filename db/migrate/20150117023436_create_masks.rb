class CreateMasks < ActiveRecord::Migration
  def change
    create_table :masks do |t|
      t.references :enterprise, index: true
      t.string :imageUrl

      t.timestamps null: false
    end
    add_foreign_key :masks, :enterprises
  end
end
