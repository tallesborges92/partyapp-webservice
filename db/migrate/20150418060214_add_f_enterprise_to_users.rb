class AddFEnterpriseToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :enterprise, index: true
    add_foreign_key :users, :enterprises
  end
end
