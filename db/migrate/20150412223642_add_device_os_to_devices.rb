class AddDeviceOsToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :device_os, :string
  end
end
