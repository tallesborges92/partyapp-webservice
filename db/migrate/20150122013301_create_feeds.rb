class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.references :enterprise, index: true
      t.date :day
      t.string :image_url
      t.string :description

      t.timestamps null: false
    end
    add_foreign_key :feeds, :enterprises
  end
end
