class ChangeEventNameAgendas < ActiveRecord::Migration
  def change
    rename_column :agendas, :agenda_name, :event_name
  end
end
