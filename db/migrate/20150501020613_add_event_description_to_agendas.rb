class AddEventDescriptionToAgendas < ActiveRecord::Migration
  def change
    add_column :agendas, :event_description, :string
  end
end
