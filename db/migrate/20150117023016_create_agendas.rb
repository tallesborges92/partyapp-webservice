class CreateAgendas < ActiveRecord::Migration
  def change
    create_table :agendas do |t|
      t.references :enterprise, index: true
      t.date :day
      t.string :description
      t.string :imageUrl

      t.timestamps null: false
    end
    add_foreign_key :agendas, :enterprises
  end
end
