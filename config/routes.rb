Rails.application.routes.draw do

  root :to => redirect('/login')

  constraints :format => 'html' do
    as :user do
      get 'login' => 'devise/sessions#new'
    end

    devise_for :users, only: :sessions
    devise_for :admins, only: :sessions

    authenticate :admin do

      devise_for :users, skip: :sessions
      devise_for :admins, skip: :sessions
      resources :enterprises, except: [:edit, :update]
    end

    authenticate :user do
      resources :notifications
      resources :devices
      resources :feeds
      resources :masks
      resources :agendas
      resources :enterprises, only: [:edit, :update]
      get 'info' => 'enterprises#edit', as: :info
    end
  end

  apipie

  namespace :api, defaults: {format: 'json'} do
    get 'enterprises/:enterprise_id/feed' => 'enterprises#feed'
    get 'enterprises/:enterprise_id/agenda' => 'enterprises#agenda'
    get 'enterprises/:enterprise_id/masks' => 'enterprises#masks'
    get 'enterprises/:enterprise_id/notifications' => 'enterprises#notifications'
    get 'enterprises/:enterprise_id/info' => 'enterprises#info'
    post 'enterprises/:enterprise_id/devices' => 'enterprises#new_device'
  end
end
