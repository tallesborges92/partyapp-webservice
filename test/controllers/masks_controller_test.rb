require 'test_helper'

class MasksControllerTest < ActionController::TestCase
  setup do
    @mask = masks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:masks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mask" do
    assert_difference('Mask.count') do
      post :create, mask: { enterprise_id: @mask.enterprise_id, imageUrl: @mask.imageUrl }
    end

    assert_redirected_to mask_path(assigns(:mask))
  end

  test "should show mask" do
    get :show, id: @mask
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mask
    assert_response :success
  end

  test "should update mask" do
    patch :update, id: @mask, mask: { enterprise_id: @mask.enterprise_id, imageUrl: @mask.imageUrl }
    assert_redirected_to mask_path(assigns(:mask))
  end

  test "should destroy mask" do
    assert_difference('Mask.count', -1) do
      delete :destroy, id: @mask
    end

    assert_redirected_to masks_path
  end
end
