require 'test_helper'

class Api::EnterprisesControllerTest < ActionController::TestCase
  test "should get feed" do
    get :feed
    assert_response :success
  end

  test "should get agenda" do
    get :agenda
    assert_response :success
  end

  test "should get masks" do
    get :masks
    assert_response :success
  end

  test "should get notifications" do
    get :notifications
    assert_response :success
  end

  test "should get info" do
    get :info
    assert_response :success
  end

end
