class Enterprise < ActiveRecord::Base
  has_many :agendas
  has_many :masks
  has_many :feeds
  has_many :notifications
end
