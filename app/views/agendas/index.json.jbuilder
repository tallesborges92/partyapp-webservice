json.array!(@agendas) do |agenda|
  json.extract! agenda, :id, :enterprise_id, :day, :description, :imageUrl
  json.url agenda_url(agenda, format: :json)
end
