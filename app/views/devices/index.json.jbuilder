json.array!(@devices) do |device|
  json.extract! device, :id, :enterpise_id, :token
  json.url device_url(device, format: :json)
end
