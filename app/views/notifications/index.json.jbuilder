json.array!(@notifications) do |notification|
  json.extract! notification, :id, :enterprise_id, :text, :date
  json.url notification_url(notification, format: :json)
end
