json.agenda @agendas do |agenda|
  json.extract! agenda, :id, :enterprise_id, :start_date, :end_date, :event_name, :description, :event_description
  json.image_url agenda.imageUrl
end

json.total_pages @agendas.total_pages