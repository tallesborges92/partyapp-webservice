json.array!(@masks) do |mask|
  json.extract! mask, :id, :enterprise_id
  json.image_url mask.imageUrl
  json.thumb_url mask.thumbUrl
end
