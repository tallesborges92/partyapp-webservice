json.array!(@feeds) do |feed|
  json.extract! feed, :id, :enterprise_id, :day, :image_url, :description
  json.url feed_url(feed, format: :json)
end
