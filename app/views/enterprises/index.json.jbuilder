json.array!(@enterprises) do |enterprise|
  json.extract! enterprise, :id, :name, :addres, :logoUrl, :telephone, :description, :hashtag
  json.url enterprise_url(enterprise, format: :json)
end
