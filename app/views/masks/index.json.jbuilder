json.array!(@masks) do |mask|
  json.extract! mask, :id, :enterprise_id, :imageUrl, :thumbUrl
  json.url mask_url(mask, format: :json)
end
