class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]

  # GET /notifications
  # GET /notifications.json
  def index
    # @notifications = Notification.order(date: :asc)
    @notifications = current_user.enterprise.notifications.order(date: :asc)
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
  end

  # GET /notifications/new
  def new
    @notification = Notification.new
  end

  # GET /notifications/1/edit
  def edit
  end

  # POST /notifications
  # POST /notifications.json
  def create
    @notification = Notification.new(notification_params)
    @notification.date = DateTime.now
    @notification.enterprise = current_user.enterprise

    respond_to do |format|
      if @notification.save

        sns = Aws::SNS::Client.new
        iphone_notification = {
            aps: {
                alert: notification_params[:text], sound: "default", badge: 1
            }
        }

        sns_message = {
            default: notification_params[:text],
            APNS_SANDBOX: iphone_notification.to_json,
            APNS: iphone_notification.to_json
        }

        sns.publish(topic_arn: 'arn:aws:sns:sa-east-1:831650654153:shed_app', message: sns_message.to_json, message_structure:"json")

        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render :show, status: :created, location: @notification }
      else
        format.html { render :new }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notifications/1
  # PATCH/PUT /notifications/1.json
  def update
    respond_to do |format|
      if @notification.update(notification_params)
        format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @notification }
      else
        format.html { render :edit }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification.destroy
    respond_to do |format|
      format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:enterprise_id, :text, :date)
    end
end
