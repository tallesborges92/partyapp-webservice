class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :layout


  protected
  def after_sign_in_path_for(resource)
    # return the path based on resource
    if resource.is_a?(User)
      feeds_path
    elsif resource.is_a?(Admin)
      enterprises_path
    end
  end

  def after_sign_out_path_for(resource)
    '/login'
  end

  private
  def layout
    is_a?(Devise::SessionsController) ? false : 'application'
  end
end
