class DevicesController < ApplicationController
  before_action :set_device, only: [:show, :edit, :update, :destroy]

  # GET /devices
  # GET /devices.json
  def index
    @devices = Device.all
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
  end

  # GET /devices/new
  def new
    @device = Device.new
  end

  # GET /devices/1/edit
  def edit
  end

  # POST /devices
  # POST /devices.json
  def create
    @device = Device.new(device_params)

    respond_to do |format|
      if @device.save
        sns = Aws::SNS::Client.new()

        arn = nil

        if device_params[:os] == 'android'
          arn = 'arn:aws:sns:sa-east-1:831650654153:app/GCM/SHED_APP_ANDROID'
        elsif device_params[:os] == 'ios'
          arn = 'arn:aws:sns:sa-east-1:831650654153:app/APNS_SANDBOX/SHED_APP'
        end

        resp_arn = sns.create_platform_endpoint(
            platform_application_arn:arn,
            token:device_params[:token]
        )

        resp = sns.subscribe(
            topic_arn: 'arn:aws:sns:sa-east-1:831650654153:shed_app',
            protocol: 'application',
            endpoint: resp_arn[:endpoint_arn],
        )

        format.html { redirect_to @device, notice: 'Device was successfully created.' }
        format.json { render :show, status: :created, location: @device }
      else
        format.html { render :new }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /devices/1
  # PATCH/PUT /devices/1.json
  def update
    respond_to do |format|
      if @device.update(device_params)
        format.html { redirect_to @device, notice: 'Device was successfully updated.' }
        format.json { render :show, status: :ok, location: @device }
      else
        format.html { render :edit }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
  def destroy
    @device.destroy
    respond_to do |format|
      format.html { redirect_to devices_url, notice: 'Device was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_device
    @device = Device.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def device_params
    params.require(:device).permit(:enterpise_id, :token, :os)
  end
end
