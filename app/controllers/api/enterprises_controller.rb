include ActionView::Helpers::TagHelper
class Api::EnterprisesController < Api::ApiApplicationController

  before_action :set_enterprise
  respond_to :json

  def_param_group :pagination do
    param :page, Integer, :desc => 'Page of Records, starting at 1'
  end

  api :GET, '/enterprises/:enterprise_id/feed', 'Show Feed of :enterprise_id'
  param_group :pagination
  example '{ "feed" : [{ "id": 1, "enterprise_id": 1, "day": "2015-02-11", "image_url": "url text", "description": "text text" }], "total_pages" : 1 }'
  example '{ "feed" : [], "total_pages" : 1 }'
  def feed
    @feeds = Array.new
    @graph = Koala::Facebook::API.new('486105018189795|4l8YCmHLOomSpdZ_M6gFJWnyfZo')
    feeds = @graph.get_connections('WoodsCuritiba', 'posts', {fields: %w(object_id message story created_time full_picture type), limit:100})
    feeds.each{ |f|
      if f['type'] != 'event' then
        feed = Feed.new(id:f['object_id'].to_s.first(9).to_i,day:f['created_time'],image_url:f['full_picture'],description:f['message'].nil? ? f['story'] : f['message'] )
        @feeds.push(feed)
      end
    }

    # @feeds = @enterprise.feeds.order(day: :desc).page params[:page]
  end

  api :GET, '/enterprises/:enterprise_id/agenda', 'Show Agenda of :enterprise_id'
  param_group :pagination
  example '{ "agenda" : [ { "id": 1, "enterprise_id": 1, "start_date": "2015-04-02T04:07:00.000Z", "end_date": "2015-04-02T04:07:00.000Z", "description": "text", "event_description": "text" , "event_name": "text" ,"image_url": " url text" } ], "total_pages" : 1 }'
  example '{ "agenda" : [], "total_pages" : 1 }'
  def agenda
    @agendas = @enterprise.agendas.order(end_date: :desc).page params[:page]
  end

  api :GET, '/enterprises/:enterprise_id/notifications', 'Show Notifications of :enterprise_id'
  param_group :pagination
  example '{ "notifications" : [ { "id": 16, "enterprise_id": 1, "text": "text text ", "date": "2015-04-02T04:07:00.000Z" } ], "total_pages" : 1 }'
  example '{ "notifications" : [], "total_pages" : 1 }'
  def notifications
    @notifications = @enterprise.notifications.page params[:page]
  end

  api :GET, '/enterprises/:enterprise_id/masks', 'Show Masks of :enterprise_id'
  example '[ { "id": 1, "enterprise_id": 1, "image_url": "url text", "thumb_url": " url text" } ]'
  example '[]'
  def masks
    @masks = @enterprise.masks
  end

  api :GET, '/enterprises/:enterprise_id/info', 'Show Info of :enterprise_id'
  example '{ "id": 1, "name": "Shed", "addres": "", "telephone": "", "description": "", "website": "www.google.com.br", "hashtag": "", "lat": -25.44498, "lng": -49.29449, "logo_url": "" , "club_image": "http://..."}'
  def info
  end

  api :POST, '/enterprises/:enterprise_id/devices', 'Register new device'
  param :token, String, :desc => 'Token device', :required => true
  param :device_os, %w(android ios), :desc => 'Device OS', :required => true
  def new_device
    device = Device.new(:enterpise_id => params[:enterprise_id], :device_os => params[:device_os], :token => params[:token])
    @device = Device.where(:token => device.token).first

    if @device.nil?
      @device = device
      if @device.save
        register_device(@device)
      else
        render json: @device.errors, status: :unprocessable_entity
      end
    else
      register_device(@device)
    end

    render json: @device, status: :ok
  end

  private
  def set_enterprise
    @enterprise = Enterprise.find(params[:enterprise_id])
  end

  def register_device(device)
    sns = Aws::SNS::Client.new()

    arn = nil

    if device.device_os == 'android'
      arn = 'arn:aws:sns:sa-east-1:831650654153:app/GCM/SHED_APP_ANDROID'
    elsif device.device_os == 'ios'
      arn = 'arn:aws:sns:sa-east-1:831650654153:app/APNS_SANDBOX/SHED_APP'
    end


    unless arn.nil?
      resp_arn = sns.create_platform_endpoint(
          platform_application_arn: arn,
          token: device.token
      )
      resp = sns.subscribe(
          topic_arn: 'arn:aws:sns:sa-east-1:831650654153:shed_app',
          protocol: 'application',
          endpoint: resp_arn[:endpoint_arn],
      )
    end
  end

end
